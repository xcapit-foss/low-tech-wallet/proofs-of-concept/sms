## Twilio

### Setup

Para levantar el servicio debemos correr los siguientes comandos
```
flask --app twilio/flask_server.py run --host=0.0.0.0 --port 8080
ngrok http 8080 --host-header="localhost:8080"
```

También se debe verificar que en la dashboard de Twilio que el numero asignado por twilio responda a al webhook levantado con el ngrok.

![Twilio Admin Image](twilio_admin.png)

Probamos:
- Enviar SMS desde [MessagingService](twilio/send_by_messaging_service.py) y desde [numero otorgado por twilio](twilio/send_by_messaging_service.py)
- [Recibir un SMS y responder](twilio/flask_server.py) en base al mensaje recibido 

### Observaciones

Se observó que recibir y responder un mensaje funciona desde las compañías Claro y Movistar. Para Personal vimos que no funciona, directamente el proveedor no envía el mensaje (Desde uno de los celulares vimos el error `error 0: No se pudo enviar el mensaje con Personal`).   

### Aclaración

El numero utilizado para recibir y responder es de Estados Unidos
