from flask import Flask, request, redirect
from twilio.twiml.messaging_response import MessagingResponse

app = Flask(__name__)

@app.route("/sms", methods=['GET', 'POST'])
def sms_reply():
    """Respond to incoming calls with a simple text message."""
    # Start our TwiML response
    body = request.values.get('Body', None)
    print('VALUES', request.values)
    resp = MessagingResponse()

    # Add a message
    if body == '1': 
       resp.message("Your balance is 12345")
    elif body == '2':
        resp.message("Buy option is not available")
    else:
        resp.message("Option not valid")

    return str(resp)

if __name__ == "__main__":
    app.run(debug=True)